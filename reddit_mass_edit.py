"""
    Copyright (C) 2016  IrrelevantPenguin@GitHub.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import praw
import argparse

class Snoo:
    def __init__(self):
        self.reddit = praw.Reddit('reddit_script')

    def login(self, username):
        self.reddit.read_only=False
        self.reddit.login(username)
 
    def extract_subreddits_to_file(self):
        for subreddits in self.reddit.get_my_subreddits(limit=None):
            with open('subscriptions.txt', 'a+') as subscriptions:
                subscriptions.write(str(subreddits) + ',\n')

    def get_comments(self):
        for comments in self.reddit.user.get_comments(limit=1000):
            print(comments)
    
    def edit_all_comments(self, text):
        for comments in self.reddit.user.get_comments(limit=1000):
            comments.edit(text)

    def delete_all_submissions(self):
        for submissions in self.reddit.user.get_submitted():
            submissions.delete()

    def subscribe(self):
        with open('/home/orange/Dropbox/subscriptions.txt', 'r') as subscription_list:
            for subreddits in subscription_list:
                self.reddit.subscribe(subreddits)
            

def main():
    parser = argparse.ArgumentParser(description='Script to mass edit all comments of a user. reddit_mass_edit.py Copyright (C) 2016  IrrelevantPenguin@Github. This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions')

    parser.usage='-u/--username <REDDIT USERNAME> -t/--text <TEXT TO EDIT COMMENT>'
    parser.add_argument('-u', '--username', help='Specify a username.', required=True)
    parser.add_argument('-t', '--text', help='Specify text for comment edits', required=True)
 
    args = parser.parse_args()
    username=args.username
    text=args.text

    snoo = Snoo()
    snoo.login(username)
    #snoo.get_comments()
    #snoo.edit_all_comments(text)
    #snoo.get_comments()
    #snoo.delete_all_submissions()
    snoo.subscribe()
        
if __name__ == '__main__':
    main()
